# Symfony

## installations
Generate new Project 
```sh
symfony new my_project
```

install Makerbundle
```sh
composer require --dev symfony/maker-bundle
```

security Bundle
```sh
composer require symfony/security-bundle
```

orm
```
composer require orm
```

[components installation](https://symfony.com/download#install-symfony-components)


## JWT-Authentication
https://www.binaryboxtuts.com/php-tutorials/symfony-6-json-web-tokenjwt-authentication/

- OpenSsl windows
https://sourceforge.net/projects/openssl-for-windows/